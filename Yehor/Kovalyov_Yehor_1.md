
__Author__ 

Kovalyov Yehor 

__Creation Date__  

01/21/2019     
        
__Environment__  

Ubuntu 18.04, Chrome Version 71

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Ability to switch currency in header 

__Objective__

This test checks whether the user will be able to see prices in euros after clicking on the "Currency" button and selecting the euro item from the drop-down list. 

__Precondition__

1. Precondition: Open http://dp-151.zzz.com.ua/ 


__Test steps__

1. Click on the "Сurrency" button
2. In the drop-down list select "Euro"

__Expected result__

1. Drop-down list is shown 
2. Page refreshed 
3. Prices are displayed in euros


__Test data__
 

__Post conditions__ 


__Pass/fail/blocked__

 





 









               
           
                        