__Author__ 

Kovalyov Yehor 

__Creation Date__  

01/21/2019     
        
__Environment__  

Ubuntu 18.04, Chrome Version 71

__Test Type__ 

Functional 

__Test Cases Title__ 

Сommunication with the site administrator

__Objective__

This test case verifies that when a user clicks a contact, we will be able to see the fields for entering information and send a question to the administrator 

__Precondition__

1. Precondition: Open http://dp-151.zzz.com.ua/ 


__Test steps__

1. Click on the "Contact us" button 
2. Opening link: http://dp-151.zzz.com.ua/index.php?route=information/contact 
3. In field "Your name" input name 
4. In field "E-Mail Address" input correct e-mail 
5. In field "Enquiry" input some text 
6. Click on "Submit" button 
7. Opening link http://dp-151.zzz.com.ua/index.php?route=information/contact/success with information about successfull enquiry

__Expected result__

1. Contact us page is shown 
2. The successfull enquiry page is shown


__Test data__
 
Data for the 3 step:  name: Yehor Kovalyov
Data for the 4 step: e-mail: bingobingo1997@gmail.com 
Data for the 5 step: enquiry: Some text was written

__Post conditions__ 


__Pass/fail/blocked__








               
           
                        