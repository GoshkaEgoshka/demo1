__Author__ 

Kovalyov Yehor 

__Creation Date__  

01/22/2019     
        
__Environment__  

Ubuntu 18.04, Chrome Version 71

__Test Type__ 

Functional 

__Test Cases Title__ 

Check the wish list products

__Objective__

This test case verifies the work of wish list, we will be able to see products in wish list 

__Precondition__

1. Go to http://dp-151.zzz.com.ua/index.php?route=common/home 
2. User must not be registered 
3. Add 2 products to wish list


__Test steps__

1. Click on Wish list button 
2. Open Wish list page 
3. See the wish list

__Expected result__

1. After Click on Wish list button link registration page 
2. Registration page is shown

__Test data__
 

__Post conditions__ 


__Pass/fail/blocked__








               
           
                        