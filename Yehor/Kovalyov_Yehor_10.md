__Author__ 

Kovalyov Yehor 

__Creation Date__  

01/23/2019     
        
__Environment__  

Ubuntu 18.04, Chrome Version 71

__Test Type__ 

Functional 

__Test Cases Title__ 

Check Navigation bar "Desktops" elements

__Objective__

This test case verifies that user can link PC page

__Precondition__

1. Go to http://dp-151.zzz.com.ua/index.php?route=common/home

__Test steps__

1. Hover on item "Desktops" in Navigation bar 
2. In drop-down list click on "PC"

__Expected result__

1. Drop-down list is shown 
2. PC page is shown

__Test data__
 

__Post conditions__ 


__Pass/fail/blocked__








               
           
                        