__Author__ 

Kovalyov Yehor 

__Creation Date__  

01/24/2019     
        
__Environment__  

Ubuntu 18.04, Chrome Version 71

__Test Type__ 

Functional 

__Test Cases Title__ 

Check Search field

__Objective__

This test case verifies that the user can find what he is looking for

__Precondition__

1. Go to http://dp-151.zzz.com.ua/index.php?route=common/home

__Test steps__

1. Click on search field 
2. Input product name
3. Click on "Search" button

__Expected result__

1. Page with the desired product is shown

__Test data__

In step 2: Mac

__Post conditions__ 


__Pass/fail/blocked__








               
           
                        