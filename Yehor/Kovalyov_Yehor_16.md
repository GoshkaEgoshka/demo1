__Author__ 

Kovalyov Yehor 

__Creation Date__  

01/27/2019     
        
__Environment__  

Ubuntu 18.04, Chrome Version 71

__Test Type__ 

Functional 

__Test Cases Title__ 

Check Navigation bar "Laptops&Notebooks" elements

__Objective__

This test case verifies that the user can display all "Laptops and Notebooks" in this category.
__Precondition__

1. Go to http://dp-151.zzz.com.ua/index.php?route=common/home

__Test steps__

1. Hover on item "Laptops&Notebooks" in Navigation bar 
2. In drop-down list click on "Show all Laptops&Notebooks"

__Expected result__

1. Drop-down list is shown 
2. All Laptops&Notebooks page is shown

__Test data__
 

__Post conditions__ 


__Pass/fail/blocked__








               
           
                        
