__Author__ 

Kovalyov Yehor 

__Creation Date__  

01/23/2019     
        
__Environment__  

Ubuntu 18.04, Chrome Version 71

__Test Type__ 

Functional 

__Test Cases Title__ 

Check "Shopping cart" button

__Objective__

This test case verifies that user can link on Shoping cart page

__Precondition__

1. Go to http://dp-151.zzz.com.ua/index.php?route=common/home 
2. Choose 2 products in shoping cart

__Test steps__

1. Click on Shoping cart button

__Expected result__

1. Shoping cart list is shown

__Test data__
 

__Post conditions__ 


__Pass/fail/blocked__








               
           
                        