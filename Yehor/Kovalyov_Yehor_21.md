__Author__ 

Kovalyov Yehor 

__Creation Date__  

01/27/2019     
        
__Environment__  

Ubuntu 18.04, Chrome Version 71

__Test Type__ 

Functional 

__Test Cases Title__ 

Check Navigation bar "Components" elements

__Objective__

This test case verifies that the user can link on Web Cameras page

__Precondition__

1. Go to http://dp-151.zzz.com.ua/index.php?route=common/home

__Test steps__

1. Hover on item "Components" in Navigation bar 
2. In drop-down list click on "Web Cameras"

__Expected result__

1. Drop-down list is shown 
2. Web Cameras page is shown

__Test data__
 

__Post conditions__ 


__Pass/fail/blocked__








               
           
                        
