 __Author__ 

Kovalyov Yehor 

__Creation Date__  

01/23/2019     
        
__Environment__  

Ubuntu 18.04, Chrome Version 71

__Test Type__ 

Functional 

__Test Cases Title__ 

Check "My account" button

__Objective__

This test case verifies that ungistrated user can link on Login page

__Precondition__

1. Go to http://dp-151.zzz.com.ua/index.php?route=common/home 
2. User must be logout


__Test steps__

1. Click on My account button 
2. Open drop-down list 
3. Choose "Log in" in drop-down list 

__Expected result__

1. Drop-down list is shown 
2. Login page is shown

__Test data__
 

__Post conditions__ 


__Pass/fail/blocked__








               
           
                        