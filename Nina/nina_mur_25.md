
__Author__ 

Nina Murashevych

__Creation Date__  

01/26/2019     
        
__Environment__  

Win 10, Chrome Version 71 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Pagination function for authorization user

__Objective__

Testing pagination function for authorization user

__Precondition__

1. Open http://dp-151.zzz.com.ua/
2. Authorization

__Test steps__

1. Click on category in navigation bar
2. In drop-down list select "Show all"
3. Verify block with all categories of products on page with id="column-left"
4. Click to category
5. Click to subcategory if it's on the category

__Expected result__

Selected category should be opened with right products from this category

__Test data__
 
 1 step. Categories for navigation bar: Desktops, Laptops & Notebooks, Components, Tablets, Software, Phones & PDAs, Cameras, MP3 Players
 4 step. Categories: 
  1. "Desktops". 
  2. "Laptops & Notebooks". 
  3. "Tablets". 
  4. "Software". 
  5. "Phones & PDAs". 
  6. "Cameras". 
  7. "MP3 Players"

__Post conditions__ 

Logout

__Pass/fail/blocked__