__Author__ 

Nina Murashevych

__Creation Date__  

01/26/2019     
        
__Environment__  

Win 10, Chrome Version 71 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Sorting function in list of product for authorization user

__Objective__

Testing possibility to sort products in list for authorization user

__Precondition__

1. Open http://dp-151.zzz.com.ua/
2. Authorization

__Test steps__

1. Click on category in navigation bar
2. In drop-down list select "Show all"
3. Click to button List with id="list-view"
4. In "Sorting By" drop-down select item.

__Expected result__

Sorting of products must be consistent with selected items

__Test data__
 
 1 step. Categories for navigation bar: Desktops, Laptops & Notebooks, Components, Tablets, Software, Phones & PDAs, Cameras, MP3 Players
 4 step. items for sorting: 
  1. Default. 
  2. Name A-Z. 
  3. Name Z-A. 
  4. Price Low-High. 
  5. Price High-Low. 
  6. Rating Highest. 
  7. Rating Lowest. 
  8. Model A-Z. 
  9. Model Z-A.

__Post conditions__ 

Logout

__Pass/fail/blocked__
