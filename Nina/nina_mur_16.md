
__Author__ 

Nina Murashevych

__Creation Date__  

01/26/2019     
        
__Environment__  

Win 10, Chrome Version 71 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

In main category Grid clicking on photo of product

__Objective__

Testing possibility to select product by clicking on photo of product from Grid

__Precondition__

1. Open http://dp-151.zzz.com.ua/

__Test steps__

1. Click on category in navigation bar
2. In drop-down list select "Show all"
3. Click to button Grid with id="grid-view"
4. Click to photo of product

__Expected result__

Page with selected product should be opened

__Test data__
 
 1 step. Categories for navigation bar: Desktops, Laptops & Notebooks, Components, Tablets, Software, Phones & PDAs, Cameras, MP3 Players
 4 step. photo with tag <a>

__Post conditions__ 


__Pass/fail/blocked__