
__Author__ 

Nina Murashevych

__Creation Date__  

01/26/2019     
        
__Environment__  

Win 10, Chrome Version 71 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

List ordering function on page of products for authorization user.

__Objective__

Testing list ordering function for authorization user

__Precondition__

1. Open http://dp-151.zzz.com.ua/
2. Authorization 

__Test steps__

1. Click on category in navigation bar
2. In drop-down list select "Show all"
3. Click to button List with id=“list-view”

__Expected result__

Sorting of products on page should be in list

__Test data__
 
 1 step. Categories for navigation bar: Desktops, Laptops & Notebooks, Components, Tablets, Software, Phones & PDAs, Cameras, MP3 Players

__Post conditions__ 

Logout

__Pass/fail/blocked__
