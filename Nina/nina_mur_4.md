
__Author__ 

Nina Murashevych

__Creation Date__  

01/26/2019     
        
__Environment__  

Win 10, Chrome Version 71 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

In Featured list clicking on photo of product for autorization user

__Objective__

Cliking on photos in featured list of products. 

__Precondition__

1. Open http://dp-151.zzz.com.ua/ 
2. Authorization.
3. Scroll to Featured list on Home page

__Test steps__

1. Click to photo of product

__Expected result__

Selected product should be opened.

__Test data__
 
 Data for the 1 step: photos of products Macbook, iphone, apple cinema, Canon.

__Post conditions__ 

Logout

__Pass/fail/blocked__
