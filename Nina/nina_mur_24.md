
__Author__ 

Nina Murashevych

__Creation Date__  

01/26/2019     
        
__Environment__  

Win 10, Chrome Version 71 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Show function in Grid of product

__Objective__

Testing possibility to show selected quantity of products in Grid

__Precondition__

1. Open http://dp-151.zzz.com.ua/

__Test steps__

1. Click on category in navigation bar
2. In drop-down list select "Show all"
3. Click to button Grid with id="grid-view"
4. In "Show" drop-down select item

__Expected result__

Quantity of products on page should be changed agree with selected item

__Test data__
 
 1 step. Categories for navigation bar: Desktops, Laptops & Notebooks, Components, Tablets, Software, Phones & PDAs, Cameras, MP3 Players
 4 step. items for show: 
  1. 15 
  2. 25 
  3. 50 
  4. 75 
  5. 100 

__Post conditions__ 


__Pass/fail/blocked__
