__Author__ 

Aleksey Didilov 

__Creation Date__  

01/19/2019     
        
__Environment__  

macOs Mojave v. 10.14.2, Google Chrome v. 71.0.3578.98 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Verify that "Wish list" page is shown after clicking the �Wish list� link. 

__Objective__

This test case verifies that an user will be able to see wish list with products after redirection from the "Account" page using "Wish list" link" in right sidebar.

__Precondition__

1. Website "Opencart" is opened on the "Home" page (link - http://dp-151.zzz.com.ua/).
2. User has already registered (email: didilov.aleksey@gmail.com; password: 12345678).
3. User has logged into account



__Test steps__

1. Click on the "account" drop-down menu in the website header.        
2. Click on the "My account" link.
3. Click on the "Wish list" link in right sidebar.
4. Osbserve the "Wish list" page is shown.

__Expected result__

1. Links in the "Account" drop-down menu are shown.  
2. The "My account" page is shown.
3. The "Wish list" page is shown.

__Test data__

1. In step 4 - email: didilov.aleksey@gmail.com 
2. In step 5 - password: 12345678

__Post conditions__ 


__Pass/fail/blocked__