__Author__ 

Aleksey Didilov 

__Creation Date__  

01/20/2019     
        
__Environment__  

macOs Mojave v. 10.14.2, Google Chrome v. 71.0.3578.98 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Verify that the "Continue" button redirects on the "Account" page from the "Wish list" page. 

__Objective__

This test case verifies that the "Continue" button redirects on the "Account" page from the "Wish list" page.

__Precondition__

1. Website "Opencart" is opened on the "Home" page (link - http://dp-151.zzz.com.ua/).
2. User has already registered (email: didilov.aleksey@gmail.com; password: 12345678).
3. User has loged into account.

__Test steps__

1. Click on the "Wish list" link in the website header.        
2. Click on the "Continue" button.
3. Osbserve the "Account" page is shown.

__Expected result__

1. The "Wish list" page is shown. 
2. The "Account" page is shown. 


__Test data__


__Post conditions__ 


__Pass/fail/blocked__