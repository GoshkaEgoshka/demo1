__Author__ 

Aleksey Didilov 

__Creation Date__  

01/22/2019     
        
__Environment__  

macOs Mojave v. 10.14.2, Google Chrome v. 71.0.3578.98 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Verify that products can be added to the "Cart" and shown in the "Cart" drop down menu for unlogged user.

__Objective__

This test case verifies that products can be added from the "Home" page to the "Cart" and shown in the "Cart" drop-down menu for unlogged user.

__Precondition__

1. Website "Opencart" is opened on the "Home" page (link - http://dp-151.zzz.com.ua/).
2. User hasn't logged into account.

__Test steps__

1. Choose some product card and click on the "Add to cart" button.        
2. Observe the "Success: You have added Product_name to your shopping cart" message is shown. Amount of added products became more on 1 item on the "Cart" drop-down menu, price for added product is shown on the "Cart" drop-down menu.

__Expected result__

1. The "Success: You have added Product_name to your shopping cart" message is shown. Amount of added products became more on 1 item on the "Cart" drop-down menu. Price for added product is shown on the "Cart" drop-down menu.

__Test data__


__Post conditions__ 


__Pass/fail/blocked__