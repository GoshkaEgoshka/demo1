__Author__ 

Aleksey Didilov 

__Creation Date__  

01/21/2019     
        
__Environment__  

macOs Mojave v. 10.14.2, Google Chrome v. 71.0.3578.98 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Verify that added products to the "Comparison" page can be removed.

__Objective__

This test case verifies that that added products to the "Comparison" page can be removed.

__Precondition__

1. Website "Opencart" is opened on the "Home" page (link - http://dp-151.zzz.com.ua/).
2. User has already registered (email: didilov.aleksey@gmail.com; password: 12345678).
3. User has logged into account

__Test steps__

1. Choose some product card and click on the "Compare this product" button.        
2. Click on the "Product comparison" link in the " Success: You have added Product_name to your product comparison!" message. 
3. Click on the "Remove" button on the "Compare" page.
4. Observe that the product has been deleted from the "Compare" page.

__Expected result__

1. The " Success: You have added Product_name to your product comparison" message is shown. 
2. The "Compare" page is shown.
3. Product has been deleted from the "Compare" page.


__Test data__


__Post conditions__ 


__Pass/fail/blocked__