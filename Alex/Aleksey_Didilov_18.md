__Author__ 

Aleksey Didilov 

__Creation Date__  

01/21/2019     
        
__Environment__  

macOs Mojave v. 10.14.2, Google Chrome v. 71.0.3578.98 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Verify that added products to compare are shown on the "Comparison" page for an unregistered user. 

__Objective__

This test case verifies that added products to compare are shown on the "Comparison" page for an unregistered user. User added products to compare from the "Home Page".

__Precondition__

1. Website "Opencart" is opened on the "Home" page (link - http://dp-151.zzz.com.ua/).
2. User hasn't logged into account.

__Test steps__

1. Choose some product card and click on the "Compare this product" button.        
2. Choose one another product card and click on the "Compare this product" button. 
3. Click on the "Product comparison" link in the " Success: You have added Product_name to your product comparison!" message.
4. Observe that added to compare products are shown on the "Compare" page.

__Expected result__

1. The " Success: You have added Product_name to your product comparison!" message is shown. 
2. The " Success: You have added Product_name to your product comparison!" message is shown.
3. Added to compare products are shown on the "Compare" page. 


__Test data__


__Post conditions__ 


__Pass/fail/blocked__