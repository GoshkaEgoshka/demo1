__Author__ 

Aleksey Didilov 

__Creation Date__  

01/19/2019     
        
__Environment__  

macOs Mojave v. 10.14.2, Google Chrome v. 71.0.3578.98 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Verify that wish list for a logged into account user contains products added before login. 

__Objective__

This test case verifies that an user will be able to see wish list with products which were added before login into account in one session.

__Precondition__

1. Website "Opencart" is opened on the "Home" page (link - http://dp-151.zzz.com.ua/).
2. User hasn't logged into account.
3. User has alreagy registered.
4. User has added some product in the "Wish list" in previous login session.


__Test steps__
        
1. Click on the "Wish list" link in the website header.
6. Observe added in previous login session product is shown on the "Wish list" page.


__Expected result__

1. Added in previous login session product is shown on the "Wish list" page.

__Test data__

__Post conditions__ 

__Pass/fail/blocked__