__Author__ 

Aleksey Didilov 

__Creation Date__  

01/20/2019     
        
__Environment__  

macOs Mojave v. 10.14.2, Google Chrome v. 71.0.3578.98 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Verify that products can be deleted from the "Wish list" page. 

__Objective__

This test case verifies that an user will be able to delete product from the "Wish list" page.

__Precondition__

1. Website "Opencart" is opened on the "Home" page (link - http://dp-151.zzz.com.ua/).
2. User has already registered (email: didilov.aleksey@gmail.com; password: 12345678).
3. User has loged into account.

__Test steps__

1. Choose some product on the "Home" page and click on the "Heart" button.        
2. Click on the "Wish list" link in the website header.
3. Click on the "Delete" button in the "Action" column.
4. Osbserve the product was deleted from the "Wish list" page.

__Expected result__

1. The "Success: You have added Product_name to your wish list!" message is shown. Amount of added products which shown in the "Wish list" link in website header was increased on 1 unit
2. The "Wish list" page is shown. 
3. Deleted product isn't shown on "Wish list" page anymore. 


__Test data__


__Post conditions__ 


__Pass/fail/blocked__