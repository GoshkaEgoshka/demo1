__Author__ 

Aleksey Didilov 

__Creation Date__  

01/24/2019     
        
__Environment__  

macOs Mojave v. 10.14.2, Google Chrome v. 71.0.3578.98 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Verify that products are shown for an unlogged user on the "Checkout" page added from "Cart" drop-down menu.

__Objective__

This test case verifies that products are shown for an unlogged user on the "Checkout" page added from "Cart" drop-down menu.

__Precondition__

1. Website "Opencart" is opened on the "Home" page (link - http://dp-151.zzz.com.ua/).
2. User hasn't logged into account.
3. Products haven't added to cart.

__Test steps__

1. Choose some product card and click on the "Add to cart" button.        
2. Click on the "Cart" drop-down menu.
3. Click on the "Checkout" link.
4. Choose the "Guest checkout" radio batton.
5. Click on the "Next" button.
6. Fill in all mandatory fields on the Step 2.
7. Left chosen by default "My delivery and billing addresses are the same" checkbox and click on the "Continue" button on Step 3.
8. Click the "Next" button on the Step 4.
9. Click on the "I have read and agree to the Terms & Conditions" checkbox on the Step 5.
10. Click on the "Continue" button.
11. Observe added product from the "Cart" drop-down menu is shown.

__Expected result__

1. The "Success: You have added Product_name to your shopping cart" message is shown. Amount of added products became more on 1 item on the "Cart" drop-down menu. Price for added product is shown on the "Cart" drop-down menu.
2. Added product is shown in the "Cart" drop-down menu.
3. The "Checkout" page is shown.
4. The "Guest checkout" radio batton was chosen.
5. Step 2 "Billing details" is shown.
6. Filled into mandatory fields data is shown.
7. The "My delivery and billing addresses are the same" checkbox is chousen.
8. Step 4 "Delivery method" is shown.
9. Step 5 "Payment method" is shown.
10. Step 6 "Confirm Order" is shown.
11. Added product from the "Cart" drop-down menu is shown.

__Test data__
 
 1. In step 6 - First name: Ivan; Laast name: Ivanov; Email: didilov.aleksey@gmail.com; Telephone: +38099999999; Address 1: Nirinberga 10; City: Dnipro; Post code: 49000; Country: Ukraine; Region/State: Dnipropetrivs'ka Oblast'.

__Post conditions__ 


__Pass/fail/blocked__