__Author__ 

Aleksey Didilov 

__Creation Date__  

01/22/2019     
        
__Environment__  

macOs Mojave v. 10.14.2, Google Chrome v. 71.0.3578.98 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Verify that the "View  cart" link redirects from the "Cart" drop-down menu on the "Cart" page.

__Objective__

This test case verifies that the "View  cart" link redirects from the "Cart" drop-down menu on the "Cart" page.

__Precondition__

1. Website "Opencart" is opened on the "Home" page (link - http://dp-151.zzz.com.ua/).
2. User has already registered (email: didilov.aleksey@gmail.com; password: 12345678).
3. User has logged into account.

__Test steps__

1. Choose some product card and click on the "Add to cart" button.        
2. Click on the "Cart" drop-down menu.
3. Click on the "View Cart" link.
4. Observe that added product is shown on the "Cart" page.

__Expected result__

1. The "Success: You have added Product_name to your shopping cart" message is shown. Amount of added products became more on 1 item on the "Cart" drop-down menu. Price for added product is shown on the "Cart" drop-down menu.
2. Added product into "Cart" is shown on the "Cart" drop-down menu.
3. Added product is shown on the "Cart" page.

__Test data__


__Post conditions__ 


__Pass/fail/blocked__