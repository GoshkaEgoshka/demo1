
__Author__ 

Aleksey Didilov 

__Creation Date__  

01/20/2019     
        
__Environment__  

macOs Mojave v. 10.14.2, Google Chrome v. 71.0.3578.98 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Verify that the "Wish list" link which shown after adding product in the "Wish list" for an unlogged user redirects on the "Login" page. 

__Objective__

This test case verifies that "Wish list" link in the request message which shown after adding product in the "Wish list" for an unlogged user redirects on the "Login" page.

__Precondition__

1. Website "Opencart" is opened on the "Home" page (link - http://dp-151.zzz.com.ua/).
2. User hasn't logged into account.

__Test steps__

1. Choose some product card and click on the "Heart" button.        
2. Click on the "Wish list" link in the "You must login or create an account to save "Product_name" to your wish list! " message. 
3. Observe the "Login" page is shown.

__Expected result__

1. The " You must login or create an account to save "Product_name" to your wish list! " message is shown. Amount of added products which shown in the "Wish list" link in website header was increased on 1 unit. 
2. The "Login" page is shown. 


__Test data__


__Post conditions__ 


__Pass/fail/blocked__