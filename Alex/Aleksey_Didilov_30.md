__Author__ 

Aleksey Didilov 

__Creation Date__  

01/24/2019     
        
__Environment__  

macOs Mojave v. 10.14.2, Google Chrome v. 71.0.3578.98 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Verify that products are shown for a logged user on the "Checkout" page added from "Cart" drop-down menu.

__Objective__

This test case verifies that products are shown for a logged user on the "Checkout" page added from "Cart" drop-down menu.

__Precondition__

1. Website "Opencart" is opened on the "Home" page (link - http://dp-151.zzz.com.ua/).
2. User has already registered (email: didilov.aleksey@gmail.com; password: 12345678).
3. User has already added personal details bill info (First name: Ivan; Laast name: Ivanov; Email: didilov.aleksey@gmail.com; Telephone: +38099999999; Address 1: Nirinberga 10; City: Dnipro; Post code: 49000; Country: Ukraine; Region/State: Dnipropetrivs'ka Oblast').
4. User has logged into account.
5. Products haven't added to cart.

__Test steps__

1. Choose some product card and click on the "Add to cart" button.        
2. Click on the "Cart" drop-down menu.
3. Click on the "Checkout" link.
4. Left chosen "I want to use an existing address" checkbox and click on the "Continue" button on Step 2 .
5. Left chosen "I want to use an existing address" checkbox adn click on the "Continue" button on Step 3.
6. Click the "Next" button on the Step 4.
7. Click on the "I have read and agree to the Terms & Conditions" checkbox on the Step 5.
8. Click on the "Continue" button.
9. Observe added product from the "Cart" drop-down menu is shown.

__Expected result__

1. The "Success: You have added Product_name to your shopping cart" message is shown. Amount of added products became more on 1 item on the "Cart" drop-down menu. Price for added product is shown on the "Cart" drop-down menu.
2. Added product is shown in the "Cart" drop-down menu.
3. Step 2  is shown on the "Checkout" page.
4. Step 3 is shown on the "Checkout" page. The "My delivery and billing addresses are the same" checkbox is chousen.
5. Step 4 is shown on the "Checkout" page.
6. Step 5 is shown on the "Checkout" page.
7. Step 6 is shown on the "Checkout" page.
8. Added product from the "Cart" drop-down menu is shown.

__Test data__
 

__Post conditions__ 


__Pass/fail/blocked__