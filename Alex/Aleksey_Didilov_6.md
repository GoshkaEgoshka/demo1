__Author__ 

Aleksey Didilov 

__Creation Date__  

01/19/2019     
        
__Environment__  

macOs Mojave v. 10.14.2, Google Chrome v. 71.0.3578.98 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Verify that added products in the "Wish list" in previous login session are shown in new session. 

__Objective__

This test case verifies that an user will be able to see wish list with products which were added in the "Wish list" by user in previous login session.

__Precondition__

1. Website "Opencart" is opened on the "Home" page (link - http://dp-151.zzz.com.ua/).
2. User has already registered (email: didilov.aleksey@gmail.com; password: 12345678).
3. User has loged into account.
4. User has added Apple Cinema 30" into the "Wish list" and made log out. 
5. User has loged into account again.



__Test steps__

1. Click on the "Wish list" link in the website header.        
2. Observe that added in previous login session product is shown in the "Wish list" page.

__Expected result__

1. Added in previous login session product is shown in the "Wish list" page.  


__Test data__


__Post conditions__ 


__Pass/fail/blocked__