__Author__ 

Aleksey Didilov 

__Creation Date__  

01/19/2019     
        
__Environment__  

macOs Mojave v. 10.14.2, Google Chrome v. 71.0.3578.98 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Verify that products can be add in the "Wish list" from the product page. 

__Objective__

This test case verifies that products can be add in the "Wish list" from the product page.

__Precondition__

1. Website "Opencart" is opened on the "Home" page (link - http://dp-151.zzz.com.ua/).
2. User has already registered (email: didilov.aleksey@gmail.com; password: 12345678).
3. User has loged into account.


__Test steps__

1. Choose some product on the "Home" page and click on it.        
2. Click on the "Heart" button on the "Product" page.
3. Click on the "Wish list" link in the website header.
4. Osbserve that added product from the "Product" page is shown on the "Wish list" page.

__Expected result__

1. The "Product" page is shown.
2. The "Success: You have added Product_name" to your wish list!" message is shown. Amount of added products which shown in the "Wish list" link in website header was increased on 1 unit.
3. Added product from the "Product" page is shown on the "Wish list" page. 


__Test data__


__Post conditions__ 


__Pass/fail/blocked__