__Author__  
Aleksandr Dyshlevyi 

__Creation Date__   
01/21/2019     
        
__Environment__  
Windows 10,Google Chrome v. 71.0.3578.98 

__Test Type__  
Functional 
 
__Test Cases Title__  
Registration when email written without '@'

__Objective__  
This test case verify registration function when email address written without '@'

__Precondition__

1. Open: http://dp-151.zzz.com.ua/index.php?route=account/register
2. User is not logged in  
3. Agree the Privet Policy check-box
4. Use english alphabet 
5. Don't use special symbol like !#$%^&*()_+


__Test steps__

1. Fill in all fields except "Email"         
2. In "Email" field write data without symbol '@'  
3. Click "Continue" button

__Expected result__

1. All fields is fill
2. Incorrect email is written
3. Printed information message about incorrect input in the "Email" field

__Test data__


__Post conditions__  
 

__Pass/fail/blocked__  
 