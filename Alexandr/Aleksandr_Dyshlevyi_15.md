__Author__  
Aleksandr Dyshlevyi 

__Creation Date__   
01/21/2019     
        
__Environment__  
Windows 10,Google Chrome v. 71.0.3578.98 

__Test Type__  
Functional 
 
__Test Cases Title__  
Password recovery funtion

__Objective__  
This test case verify recovery forgotten password function

__Precondition__

1. Open: http://dp-151.zzz.com.ua/index.php?route=account/login
2. User is not logged in   
3. Check email. Does recovery form was arrived. 


__Test steps__

1. Click "Forgotten password" button         
2. Write email which to send recovery password form  
3. Click "Continue" button

__Expected result__

1. "Forgot your password" page is open
2. Email is written
3. Information message about sending recovery form on email is printed

__Test data__


__Post conditions__  
 

__Pass/fail/blocked__  
 