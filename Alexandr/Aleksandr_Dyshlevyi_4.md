__Author__  
Aleksandr Dyshlevyi 

__Creation Date__   
01/21/2019     
        
__Environment__  
Windows 10,Google Chrome v. 71.0.3578.98 

__Test Type__  
Functional 
 
__Test Cases Title__  
Sign in account with registered email and empty "Password" field 

__Objective__  
This test case verifies to try sign in with registered email and empty "Password" field. What message will print and how site behaves 

__Precondition__

1. Open: http://dp-151.zzz.com.ua/index.php?route=account/login
2. User should not be logged
3. User must be registered  


__Test steps__

1. In field "E-Mail Address" enter registered email        
2. The "Password" field leave empty 
3. Click on "Login" button

__Expected result__

1. Registered email is written
2. The "Password" field is empty
3. On the site should display message about incorrect input.

__Test data__


__Post conditions__  
 

__Pass/fail/blocked__  
 