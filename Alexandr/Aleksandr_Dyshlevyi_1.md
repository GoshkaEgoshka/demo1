__Author__  
Aleksandr Dyshlevyi 

__Creation Date__   
01/21/2019     
        
__Environment__  
Windows 10,Google Chrome v. 71.0.3578.98 

__Test Type__  
Functional 
 
__Test Cases Title__  
Sign in with registered login and correct password 

__Objective__  
This test case verifies ability to login with registered login and correct password. 

__Precondition__

1. Open: http://dp-151.zzz.com.ua/index.php?route=account/login
2. User should not be logged
3. User must be registered  


__Test steps__

1. In field "E-Mail Address" enter registered email
Test data:         
2. In field "Password" enter correct password
Test data: 
3. Click on "Login" button
Test data: 

__Expected result__

1. Registered email is written
2. Password is written as asterisk symbols
3. "My Account" page is opened

__Test data__
 
 
__Post conditions__  
 Log out

__Pass/fail/blocked__  
 