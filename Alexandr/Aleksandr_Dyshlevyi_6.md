__Author__  
Aleksandr Dyshlevyi 

__Creation Date__   
01/21/2019     
        
__Environment__  
Windows 10,Google Chrome v. 71.0.3578.98 

__Test Type__  
Functional 
 
__Test Cases Title__  
How many characters can take "E-Mail Address" and "Password" fields 

__Objective__  
This test case verify how many characters can take "E-Mail Address" and "Password" fields. How site behaves

__Precondition__

1. Open: http://dp-151.zzz.com.ua/index.php?route=account/login
2. User should not be logged  


__Test steps__

1. In the "E-Mail Address" field try to write as many characters as possible        
2. Сount how many characters was written in the "E-Mail Address" field
3. In the "Password" field try to write as many characters as possible
4. Сount how many characters was entered in the "Password" field

__Expected result__

1. Up to 255 characters expected in "Email" filed
2. Up to 255 characters expected in "Password" filed

__Test data__


__Post conditions__  
 

__Pass/fail/blocked__  
 