__Author__  
Aleksandr Dyshlevyi 

__Creation Date__   
01/21/2019     
        
__Environment__  
Windows 10,Google Chrome v. 71.0.3578.98 

__Test Type__  
Functional 
 
__Test Cases Title__  
Registration when all required fields is filled

__Objective__  
This test case verify registration function when only all required(marked with asterisk) fields is filled. Make sure that some optional field is not mandatory in the system

__Precondition__

1. Open: http://dp-151.zzz.com.ua/index.php?route=account/register
2. User is not logged in  
3. Agree the Privet Policy check-box
4. Use english alphabet 
5. Don't use special symbol like !@#$%^&*()_+


__Test steps__

1. Fill in all fields marked with asterisk        
2. Not marked with asterisk fields leave empty  
3. Click "Continue" button

__Expected result__

1. All marked fields is fill
2. All not marked fields is empty
3. Registration is complited.Account page is opened

__Test data__


__Post conditions__  
 Log out

__Pass/fail/blocked__  
 