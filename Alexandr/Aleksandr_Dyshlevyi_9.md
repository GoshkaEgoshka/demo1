__Author__  
Aleksandr Dyshlevyi 

__Creation Date__   
01/21/2019     
        
__Environment__  
Windows 10,Google Chrome v. 71.0.3578.98 

__Test Type__  
Functional 
 
__Test Cases Title__  
Registration when all fields is empty

__Objective__  
This test case verify registration function when all fields left empty. What message will print and how site behaves

__Precondition__

1. Open: http://dp-151.zzz.com.ua/index.php?route=account/register
2. User is not logged in  
3. Agree the Privet Policy check-box


__Test steps__

1. Leave all fields empty        
2. Click "Continue" button  

__Expected result__

1. All fields is empty
2. Printed informating message about empty fields

__Test data__


__Post conditions__  
 

__Pass/fail/blocked__  
 