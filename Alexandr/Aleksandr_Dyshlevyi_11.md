__Author__  
Aleksandr Dyshlevyi 

__Creation Date__   
01/21/2019     
        
__Environment__  
Windows 10,Google Chrome v. 71.0.3578.98 

__Test Type__  
Functional 
 
__Test Cases Title__  
Registration when all fields is filled

__Objective__  
This test case verify registration function when all  fields is filled. After registration in "My Account" check that information has been saved correctly

__Precondition__

1. Open: http://dp-151.zzz.com.ua/index.php?route=account/register
2. User is not logged in  
3. Agree the Privet Policy check-box
4. Use english alphabet 
5. Don't use special symbol like !@#$%^&*()_+


__Test steps__

1. Fill in all fields         
2. Click "Continue" button  
3. Check all written information

__Expected result__

1. All fields is fill
2. Registration is complited.Account page is opened
3. All information has been saved correctly

__Test data__


__Post conditions__  
 Log out

__Pass/fail/blocked__  
 