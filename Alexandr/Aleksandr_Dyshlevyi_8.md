__Author__  
Aleksandr Dyshlevyi 

__Creation Date__   
01/21/2019     
        
__Environment__  
Windows 10,Google Chrome v. 71.0.3578.98 

__Test Type__  
Functional 
 
__Test Cases Title__  
Log out function

__Objective__  
This test case verify log out function from "My Account" page. What message will print and how site behaves

__Precondition__

1. Open: http://dp-151.zzz.com.ua/index.php?route=account/login
2. User must be registered  
3. User must be logged


__Test steps__

1. Click "My Account" drop-down menu     
2. Click "Logout" button  

__Expected result__

1. "My Account" menu is open
2. User have been loged out
3. On site print informating message

__Test data__


__Post conditions__  
 

__Pass/fail/blocked__  
 