
__Author__ 

Pavel Havrylov

__Creation Date__  

01/25/2019     
        
__Environment__  

Win 10, Chrome Version 71 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Verify that guest user which specify valid coupon on percentage amount discount(30%) can choose the delivery address different than billing address

__Objective__

Verify the scenario when guest user specify valid coupon on percentage amount discount (30%) and after fulling "Billing Details" form remove check mark form the "
My delivery and billing addresses are the same." check-box. In this case he must specify data in the "Delivery Details" different from data in "Billing Details" form. And the this action doesn't broke normal system work (the price will be lower on 30% and confirmation message appear).

__Precondition__

1. User must be located on the cart page: http://dp-151.zzz.com.ua/index.php?route=checkout/cart with added one type(or every) of special product to the cart.
2. The list of products which are participate in the special dial: Apple - iPhone XS Max 64GB - Gold. Apple - 27 iMac Pro. Apple - 12.9-Inch iPad Pro (Latest Model) with Wi-Fi - 256GB - Space Gray(only this products can be chosen for this test).
3. User must be logged out.


__Test steps__

1. Open "Use Coupon Code" tab and enter the coupon code:GREEN
       
2. Click "Apply Coupon" and "Checkout" button

3. Change the checkout options on the "Guest Checkout" via proper radio button

4. Click "Continue" button

5. Fill "Your Personal Details" form with a valid data.

6. Remove check mark from "My delivery and billing addresses are the same." and click continue.

7. Fill the "Delivery Details" form with a valid data and click continue.  

8. Leave empty "Add Comments About Your Order" field and click "Continue" in the "Delivery method" form

9. Click the "Terms & Conditions" check-box in the Payment method field and click "Continue" button

10. Confirm your order.

__Expected result__

1. The proper code was entered in the "Use coupon code" field

2. The message " Success: Your coupon discount has been applied! was shown, user located in the checkout page.

3. User is able chose guest checkout option.

4. Your personal Details page is shown.

5. User is able to fill form with a valid data.

6. The "Delivery Details" form is shown.

7. The "Delivery Method" form is shown. 

8. The "Payment Method" form is shown.

9. The order confirmation page is shown.

10. The User get confirmation message: "Your order has been placed!"The product was ordered with a 30% discount.

__Test data__

__Post conditions__ 

__Pass/fail/blocked__