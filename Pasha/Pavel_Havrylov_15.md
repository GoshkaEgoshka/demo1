
__Author__ 

Pavel Havrylov

__Creation Date__  

01/25/2019     
        
__Environment__  

Win 10, Chrome Version 71 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Verify that checkout processes with a coupon on free shipping for a register user is working properly in situation when user want use different Billing Details

__Objective__

Verify the scenario when user already have an account, and after log in he specify valid coupon on free shipping. After he proceed to checkout he don't want use an existing address(the data that specified in account setting). He must be able to finish the order with a free shipping and new address according to steps providing in this test case.

__Precondition__

1. User must be located on the cart page: http://dp-151.zzz.com.ua/index.php?route=checkout/cart with added one type(or every) of special product to the cart.
2. The list of products which are participate in the special dial: Apple - iPhone XS Max 64GB - Gold. Apple - 27 iMac Pro. Apple - 12.9-Inch iPad Pro (Latest Model) with Wi-Fi - 256GB - Space Gray(only this products can be chosen for this test).
3. User must be login with next data - Login:green@green.com. Password:zxcvb123.


__Test steps__

1. Open "Use Coupon Code" tab and enter the coupon code:GOLYB
       
2. Click "Apply Coupon" and "Checkout" button

3. Change option to "I want to use a new address", fill the form by valid data and click "Continue".

4. Leave option "I want to use an existing address" by default and click "Continue".

5. Leave option "Flat rate shipping" by default and click "Continue".

6. Leave option "Cash on delivery" by default, mark "Terms & Conditions" as read and click "Continue".

7. Conform your order.


__Expected result__

1. The proper code was entered in the "Use coupon code" field

2. The message " Success: Your coupon discount has been applied! was shown. User located on the  "Billing details" form of checkout page.

3. The "Delivery Details " form is shown.

4. The "Delivery method" form is shown.

5. The "Payment method" form is shown. 

6. The "Order confirmation" form is show the "Total" price is calculated with a free shipping.

7. The confirmation message is shown "Your order has been placed!", the product was bought with a free shipping.

__Test data__

__Post conditions__ 

__Pass/fail/blocked__