
__Author__ 

Pavel Havrylov

__Creation Date__  

01/25/2019     
        
__Environment__  

Win 10, Chrome Version 71 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Verify that share via "Twitter" function for a guest user work properly on the product description page

__Objective__

Verify that guest user can open any product description and share this product using "Twitter" social media button

__Precondition__

1. User must located on the site: http://dp-151.zzz.com.ua.
2. User must be logged out.
3. User must disable all extension in browser witch can change visual interface, or prevent normal browser working (Adblock, Ublock etc.).


__Test steps__

1. Click on any product from the "Featured" products zone.
       
2. Click "Tweet" button in the social media menu( locate under "Add to cart" button).


__Expected result__

1. The product information page is show.

2. The new window with twitter website appears. User can share this product.

__Test data__

__Post conditions__ 

__Pass/fail/blocked__