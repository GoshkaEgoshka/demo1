
__Author__ 

Pavel Havrylov

__Creation Date__  

01/25/2019     
        
__Environment__  

Win 10, Chrome Version 71 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Verify that valid "Gift Certificate" code is work properly for the guest user

__Objective__

Verify that non register user can use a gift certificate code on the shopping cart page and price on the product decrease by the 1000$. Confirmation message will be shown:"Success: Your gift certificate discount has been applied!"

__Precondition__

1. User must be located on the cart page: http://dp-151.zzz.com.ua/index.php?route=checkout/cart with added one type(or every) of special product to the cart.
2. The list of products which are participate in the special dial: Apple - iPhone XS Max 64GB - Gold. Apple - 27 iMac Pro. Apple - 12.9-Inch iPad Pro (Latest Model) with Wi-Fi - 256GB - Space Gray(only this products can be chosen for this test).
3. User must be logged out.


__Test steps__

1. Open "Use Gift Certificate" tab and enter the coupon code:NECO.

2. Click "Apply Gift Certificate" button

__Expected result__

1. The proper code was entered in the "Use coupon code" field.

2. The confirmation message is show on the shopping cart page:" Success: Your gift certificate discount has been applied!". The total value reduced on 1000$(our case).

__Test data__

__Post conditions__ 

__Pass/fail/blocked__