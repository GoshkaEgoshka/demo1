
__Author__ 

Pavel Havrylov

__Creation Date__  

01/25/2019     
        
__Environment__  

Win 10, Chrome Version 71 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Verify that guest user can't use a free shipping coupon which intended only for register users

__Objective__

Verify the situation when non register user try use a free shipping coupon code(no minimum total amount) which intended only for register users. In this situation the error message must appear:"Warning: Coupon is either invalid, expired or reached its usage limit!".

__Precondition__

1. User must be located on the cart page: http://dp-151.zzz.com.ua/index.php?route=checkout/cart with added one type(or every) of special product to the cart.
2. The list of products which are participate in the special dial: Apple - iPhone XS Max 64GB - Gold. Apple - 27 iMac Pro. Apple - 12.9-Inch iPad Pro (Latest Model) with Wi-Fi - 256GB - Space Gray(only this products can be chosen for this test).
3. User must be logged out.


__Test steps__

1. Open "Use Coupon Code" tab and enter the coupon code:BLACK
       
2. Click "Apply Coupon" and "Checkout" button


__Expected result__

1. The proper code was entered in the "Use coupon code" field

2. If non register user try use a free shipping coupon code(with out minimum total amount) which intended only for register users he would get the error message "Warning: Coupon is either invalid, expired or reached its usage limit!".

__Test data__

__Post conditions__ 

__Pass/fail/blocked__