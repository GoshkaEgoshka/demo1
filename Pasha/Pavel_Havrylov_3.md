
__Author__ 

Pavel Havrylov

__Creation Date__  

01/25/2019     
        
__Environment__  

Win 10, Chrome Version 71 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Verify that checkout processes with a coupon on a fixed amount discount(10$) for a guest user is working properly 

__Objective__

Non register user must be able to use a coupon for getting 10$ discount. The discount option must work correctly till the moment of end ordering process and getting the confirmation message.

__Precondition__

1. User must be located on the cart page: http://dp-151.zzz.com.ua/index.php?route=checkout/cart with added one type(or every) of special product to the cart.
2. The list of products which are participate in the special dial: Apple - iPhone XS Max 64GB - Gold. Apple - 27 iMac Pro. Apple - 12.9-Inch iPad Pro (Latest Model) with Wi-Fi - 256GB - Space Gray(only this products can be chosen for this test).
3. User must be logged out.


__Test steps__

1. Open "Use Coupon Code" tab and enter the coupon code:POOR
       
2. Click "Apply Coupon" and "Checkout" button

3. Change the checkout options on the "Guest Checkout" via proper radio button

4. Click "Continue" button

5. Fill "Your Personal Details" form with a valid data and click the "Continue" button

6. Leave empty "Add Comments About Your Order" field and click "Continue" in the "Delivery method" form

7. Click the "Terms & Conditions" check-box in the Payment method field and click "Continue" button

8. Confirm your order     

__Expected result__

1. The proper code was entered in the "Use coupon code" field

2. The message " Success: Your coupon discount has been applied! was shown, user located in the checkout page.

3. User is able chose guest checkout option.

4. Your personal Details page is shown.

5. The data that we use for the form was accepted and the Delivery Detail page is shown.

6. The delivery Method page is shown.

7. The order confirmation page is shown.

8. The User get confirmation message: "Your order has been placed!". The product was bought with a 10$ discount.

__Test data__

__Post conditions__ 

__Pass/fail/blocked__