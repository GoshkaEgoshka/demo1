
__Author__ 

Liliia Kryva

__Creation Date__  

01/22/2019     
        
__Environment__  

Ubuntu 18.04, Mozilla Firefox Quantum; Windows 8.1, Opera Version 57.0.3098.116 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Adding product to the basket when the user is registered

__Objective__

The product is added to the basket on the site if user opens selected product and clicks on the "Add to cart" button on the right of the selected product when he is registered.

__Precondition__

1. User opens the site: http://dp-151.zzz.com.ua
2. User is already registered (email: janyjanet613@gmail.com; password: qw1234567)

__Test steps__

1. Select a category with goods in the navigation bar and click on the selected product. 
2. Click on the "Add to cart" button on the right of the selected product. 
3. Open the basket block which is in the top on the right of the page.

__Expected result__

1. The selected product is open.
2. The selected product was added to the basket block. 
3. The product is in the basket block. 

__Test data__

__Post conditions__ 

User logged out account.

__Pass/fail/blocked__



