
__Author__ 

Liliia Kryva

__Creation Date__  

01/22/2019     
        
__Environment__  

Ubuntu 18.04, Mozilla Firefox Quantum; Windows 8.1, Opera Version 57.0.3098.116 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Quantity of goods in the basket is updated when user adds the product from the catalog to the basket again

__Objective__

Every time in case of adding goods from the catalog to the cart, the quantity of goods is updated, when the user is registered.

__Precondition__

1. User opens the site: http://dp-151.zzz.com.ua. 
2. 2.User is already registered (email: janyjanet613@gmail.com; password: qw1234567).

__Test steps__

1. Select a category with goods in the navigation bar and click on the "Add to cart" button below the selected product. 
2. See that the amount of goods is updated when the goods are added to the basket block again.

__Expected result__

1. The product on the site is added to the basket. 
2. The amount of production is updated in the cart.

__Test data__

__Post conditions__ 

User logged out account.

__Pass/fail/blocked__


