
__Author__ 

Liliia Kryva

__Creation Date__  

01/22/2019     
        
__Environment__  

Ubuntu 18.04, Mozilla Firefox Quantum; Windows 8.1, Opera Version 57.0.3098.116 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Adding product to the basket when the user isn't registered

__Objective__

The product is added to the basket on the site after the user enters the required quantity of the selected product in the "Qty" (Quantity) field and clicks on the "Add to cart" button on the right of the selected product when he isn't registered.

__Precondition__

1. User opens the site: http://dp-151.zzz.com.ua

__Test steps__

1. Select a category with goods in the navigation bar and click on the selected product.
2. Enter the required quantity of the selected product in the "Qty"(Quantity) field on the right of the selected product. 
3. Click on the "Add to cart" button on the right of the selected product. 
4. Open the shopping cart which is in the top of the page. 
5. Check the availability of the selected product in the basket.

__Expected result__

1. The selected product is open.
2. The required quantity of the selected product is entered in the "Qty"(Quantity) field. 
3. The required quantity of the selected product is added to the cart. 
4. The shopping cart is open. 
5. The required quantity of the selected product is in the basket.

__Test data__

__Post conditions__ 

__Pass/fail/blocked__


