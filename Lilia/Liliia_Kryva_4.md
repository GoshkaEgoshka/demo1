
__Author__ 

Liliia Kryva

__Creation Date__  

01/22/2019     
        
__Environment__  

Ubuntu 18.04, Mozilla Firefox Quantum; Windows 8.1, Opera Version 57.0.3098.116 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Deletion the product from the shopping cart when user clicks on the "Remove" button in the basket block

__Objective__

The product is deleted from the shopping cart when user clicks on the "Remove" button in the basket block, when the user isn't registered.

__Precondition__

1. User opens the site: http://dp-151.zzz.com.ua

__Test steps__

1. Select a category with goods in the navigation bar and click on the "Add to cart" button below the selected product. 
2. Click on the button with items of the basket block. 
3. Click on the "Remove" button to delete the selected product.

__Expected result__

1. The product on the site is added to the basket. 
2. User sees the list of product in the basket block. 
3. The selected product is deleted.

__Test data__

__Post conditions__ 

__Pass/fail/blocked__

      
