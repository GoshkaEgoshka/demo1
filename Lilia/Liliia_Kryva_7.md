
__Author__ 

Liliia Kryva

__Creation Date__  

01/22/2019     
        
__Environment__  

Ubuntu 18.04  Mozilla Firefox Quantum; Windows 8.1, Opera Version 57.0.3098.116 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

The amount of the selected product in the basket block is recalculated when user changes the amount of the selected product in the shopping cart

__Objective__

In case of change the amount of the selected product in the shopping cart, the amount of the selected product in the basket block is recalculated when the user is registered.

__Precondition__

1. User opens the site: http://dp-151.zzz.com.ua
2. User is already registered (email: janyjanet613@gmail.com; password: qw1234567)

__Test steps__

1. Select a category with goods in the navigation bar and click on the "Add to cart" button below the selected product. 
2. Open the shopping cart which is in the top of the page.  
3. Increase the number of the selected item in the "Quantity" field in the shopping basket. 
4. Click on the "Update" button in the shopping cart.

__Expected result__

1. The product on the site is added to the basket block. 
2. The shopping cart is open. 
3. The number of the selected item was changed in the "Quantity" field in the shopping cart. 
4. The amount of the selected product is updated in the shopping cart.

__Test data__

__Post conditions__ 

User logged out account.

__Pass/fail/blocked__

