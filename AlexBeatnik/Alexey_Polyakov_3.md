
__Author__ 

Alexey Polyakov 

__Creation Date__  

01/21/2019     
        
__Environment__  

Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:64.0) Gecko/20100101 Firefox/64.0

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Checking the ability to write reviews (Login 11 symbols Review 492 symbols and Rating 'Good')

__Objective__

This test case tests the function to write a review. Login 11 symbols Review 492 symbols and Rating 'Good'

__Precondition__

1. Precondition: Open http://dp-151.zzz.com.ua/index.php?route=product/product&path=20_26&product_id=75



__Test steps__

1. Click to the 'Review' button     
2. Into Review field enter review    
3. Into 'Your Name' field enter login
4. In radiobutton 'Rating' choose good
5. Click contiune button

__Expected result__

1. Review window will open
2. In input field displayed login
3. Review text displayed in review field
4. In radobutton displayed choice good
5. The review will be displayed on the product page or will be sent for moderation.

__Test data__
 
1. login 'alexbeatnik'
2. Review 'I loved my old Mac. I really did not want to stop using it. When Comcast upgraded it just could not handle it. I was not able to get on the sights I needed to go to. Well I do love it. It is so easy to use. I am a senior and I installed it myself . It even installed my printer itself. All I had to do was plug in the printer and the Mac prompted me on what to do. It even spells for me. I do not like the mouse. It makes the pages jump, but I am sure it is just me and I will get used to it.'

__Post conditions__ 

Remove review from database if test case is pass

__Pass/fail/blocked__

 
