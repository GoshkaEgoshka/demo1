
__Author__ 

Alexey Polyakov 

__Creation Date__  

01/21/2019     
        
__Environment__  

Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:64.0) Gecko/20100101 Firefox/64.0

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Checking opening review window with a button 'Write a review'

__Objective__

This case checks the opening review window with a click on the button 'Write a review'

__Precondition__

1. Precondition: Open http://dp-151.zzz.com.ua/index.php?route=product/product&product_id=40



__Test steps__

1. Click on the button whrite a review    


__Expected result__

1. 1.The window with reviews will be open

__Test data__
 


__Post conditions__ 



__Pass/fail/blocked__

 
