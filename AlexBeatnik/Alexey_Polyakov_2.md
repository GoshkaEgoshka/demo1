
__Author__ 

Alexey Polyakov

__Creation Date__  

01/21/2019     
        
__Environment__  

Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:64.0) Gecko/20100101 Firefox/64.0|Functional 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Check that the price changes when the quantity of one product changes. 

__Objective__

This test case checks the changes that occur with the price when the quantity of goods is changed

__Precondition__

1. User should not be logged.



__Test steps__

1. Go to the website http://dp-151.zzz.com.ua/index.php?route=product/product&path=20_26&product_id=75        
2. Changing qty from 1 to 2.


__Expected result__

1. Page which product will open 
2. The price for the two products will be displayed.

__Test data__
 

__Post conditions__ 



__Pass/fail/blocked__


