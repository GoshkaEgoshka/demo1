
__Author__ 

Alexey Polyakov 

__Creation Date__  

01/22/2019    
        
__Environment__  

Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:64.0) Gecko/20100101 Firefox/64.0 

__Test Type__ 

Functional 
 
__Test Cases Title__ 

Checking the transition from the main page to the product page

__Objective__

This case tests the transition from the main page to the product page. 

__Precondition__

1. User should not be logged. 


__Test steps__

1.  Go to the website http://dp-151.zzz.com.ua/index.php?route=common/home          
2. Choose category PC
3. Choose a random product in the category PC.

__Expected result__

1. Site will open
2. Category PC will open 
3. Product page will open

__Test data__
 
 
__Post conditions__ 



__Pass/fail/blocked__

 
